<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/stianfa/footballteammanagerapi">
    <img src="images/logo.png" alt="Logo" width="250" height="150">
  </a>

  <h3 align="center">Football Team Manager API</h3>

  <p align="center">
    This is our representation of the Football Team Manager Case at Experis Academy.
    <br />
    <a href="https://footballmanagercase.web.app/"><strong>Frontend of the Football Team Manager Case</strong></a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About Football Team Manager API](#about-football-team-manager-api)
  * [Built With](#built-with)
  * [Deployed With](#deployed-with)
* [Getting Started](#getting-started)
* [Usage](#usage)
  * [Requests examples](#request-examples)
  * [POST example](#post-example)
  * [GET example](#get-example)
  * [PUT example](#put-example)
  * [DELETE example](#delete-example)
  * [Api-Endpoints](#api-endpoints)
    * [Open Endpoints](#open-endpoints) 
    * [Secured Endpoints](#secured-endpoints)
* [Authentication](#authentication)
* [Entity Framework](#entity framework)
* [Not completed requirements](#not-completed-requirements)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)


Project Link - GitLab: [https://gitlab.com/stianfa/footballteammanagerapi/tree/master](https://gitlab.com/stianfa/footballteammanagerapi/tree/master)

Deployed API: [http://ftmapi.northeurope.azurecontainer.io/api/players](http://ftmapi.northeurope.azurecontainer.io/api/players)


<!-- ABOUT THE PROJECT -->
## About Football Team Manager API

This README is the documentation for the backend and API part of the Football Team Manager case at Experis Acadamy. 

The Football Team Manager API exposes the functions of the Azure-deployed database. 
Beeing the middle-stack, the API's purpose is to connect the backend and frontend of this project. 

The API handles the requests from frontend and gives reponses accordingly to the different available methods implemented.


### Built With

* ASP .NET 
* Entity Framework Core 3
* JWT
* Identity

### Deployed With

* Azure
* Docker


<!-- GETTING STARTED -->
## Getting Started

No installation is required.
Just click on the url-link to get started. 



<!-- USAGE EXAMPLES -->
## Usage

Base API url: http://ftmapi.northeurope.azurecontainer.io/api

Example endpoint for matches: 
http://ftmapi.northeurope.azurecontainer.io/api/matches

## Requests examples

## POST example
Example code is using POST to add a new location.
<pre><code>
{
    "address": {
        "id": 2,
        "addressLine1": "Example Street",
        "addressLine2": "Optional addressline",
        "addressLine3": "Optional addressline",
        "postalCode": 1234,
        "city": "Bergen",
        "country": "Norway"
        },
    "name": "Name of the location",
    "description": "Description of the location"
}

</pre></code>

<code>
Http-request: Post
Url: http://ftmapi.northeurope.azurecontainer.io/api/locations
Status: 201 Created      
</code>

## GET example
Displays an already made instance(s) from the database (if authorized).

Auhorized request:
<code>
Http-request: Get 
Url: http://ftmapi.northeurope.azurecontainer.io/api/players/16
Status: 200 OK
</code>

Unauhorized request:
<code>
Http-request: Get 
Url: http://ftmapi.northeurope.azurecontainer.io/api/addresses
Status: 401 Unauthorized
</code>

## PUT example 
Edit an already made instance(s) from the database.

Example-url: http://ftmapi.northeurope.azurecontainer.io/api/players/1
<pre><code>
{
    "Id" : 1,
    "Person": {
    	"Id" : 2,
        "Address": {
        	"Id": 3,
            "AddressLine1": "AddressLine1Person",
            "AddressLine2": "AddressLine2Person",
            "AddressLine3": "AddressLine3Person",
            "PostalCode": 111133,
            "City": "ManchesterPerson",
            "Country": "EnglandPerson"
        },
        "FirstName": "First name",
        "LastName": "Last name",
        "DateOfBirth": "2012-04-23T18:25:43.511Z"
    },
    "Team": {
    	"Id" : 4,
        "Association": {
        	"Id" : 5,
            "Name": "*Association nameTeam*",
            "Description": "*Association descriptionTeam*"
        },
        "Coach": {
        	"Id": 6,
            "Person": {
            	"Id" : 7,
                "Address": {
                	"Id" : 8,
                    "AddressLine1": "AddressLine1CoachTeam",
                    "AddressLine2": "AddressLine2CoachTeam",
                    "AddressLine3": "AddressLine3CoachTeam",
                    "PostalCode": 11113344,
                    "City": "ManchesterCoachTeam",
                    "Country": "EnglandCoachTeam"
                },
                "FirstName": "First nameCoachTeam",
                "LastName": "Last nameCoachTeam",
                "DateOfBirth": "2012-04-23T18:25:43.511Z"
            }
        },
        "Owner": {
        	"Id" : 9,
            "Person": {
            	"Id" : 10,
                "Address": {
                	"Id" : 11,
                    "AddressLine1": "AddressLine1OwnerTeam",
                    "AddressLine2": "AddressLine2OwnerTeam",
                    "AddressLine3": "AddressLine3OwnerTeam",
                    "PostalCode": 1111335544,
                    "City": "ManchesterOwnerTeam",
                    "Country": "EnglandOwnerTeam"
                },
                "FirstName": "First nameOwner",
                "LastName": "Last nameOwner",
                "DateOfBirth": "2012-04-23T18:25:43.511Z"
            }
        },
        "Location": {
        	"Id" : 12,
            "Address": {
            	"Id" : 13,
                "AddressLine1": "AddressLine1LocationTeam",
                "AddressLine2": "AddressLine2LocationTeam",
                "AddressLine3": "AddressLine3LocationTeam",
                "PostalCode": 123456,
                "City": "Liverpool Location Team",
                "Country": "England Location Team"
            },
            "Name": "Liverpool FC",
            "Description": "Description Team"
        }
    },
    "NormalPosition" : "Striker",
    "Number" : "9"
}
</pre></code>

## DELETE example
Delete an already made instance(s) from the database

<code>
Http-request: Delete 
Url: http://ftmapi.northeurope.azurecontainer.io/api/players/1
Status: 200 OK
</code>


## API Endpoints
The API has two different types of endpoints; open and secured.
The requirements of the case given are that the API should be properly secured. 
Therefore the open endpoints are few compared to the secured ones.

## Open Endpoints

* [/Matches](http://ftmapi.northeurope.azurecontainer.io/api/matches)
* [/Players](http://ftmapi.northeurope.azurecontainer.io/api/players)
* [/Register](http://ftmapi.northeurope.azurecontainer.io/api/register)
* [/Authenticate](http://ftmapi.northeurope.azurecontainer.io/api/authenticate)


## Secured endpoints
These endpoints require a valid token (JWT) to be sent with each request.
With the exceptions of Admin and UpdateUser, all endpoints can be displayed with a GET-request. 

Admin

    [HttpPost] List<ApplicationUser> loggedInAndTarget
    
UpdateUser

    [HttpPut("email")]
    [HttpPut("password")]
    
Addresses / Addresses/{id}

    Get all addresses: http://ftmapi.northeurope.azurecontainer.io/api/addresses
    Get / Put / Delete a specific address: http://ftmapi.northeurope.azurecontainer.io/api/addresses/{id}

    
Associations / Associations/{id} 

    Get all / Post an association: http://ftmapi.northeurope.azurecontainer.io/api/associations
    Get / Put / Delete a spesific association: http://ftmapi.northeurope.azurecontainer.io/api/associations/{id}

    
Coaches / Coaches/{id}

    Get all / Post a coach: http://ftmapi.northeurope.azurecontainer.io/api/coaches
    Get / Put / Delete a spesific coach: http://ftmapi.northeurope.azurecontainer.io/api/coaches/{id}
    
Contacts / Contacts/{id}

    Get all / Post a contact: http://ftmapi.northeurope.azurecontainer.io/api/contacts
    Get / Put / Delete a spesific contact: http://ftmapi.northeurope.azurecontainer.io/api/contacts/{id}
    
GoalTypes / GoalTypes/{id}

    Get all / Post a goaltype: http://ftmapi.northeurope.azurecontainer.io/api/goaltypes
    Get / Put / Delete a spesific goaltype: http://ftmapi.northeurope.azurecontainer.io/api/goaltypes/{id}

    
Locations / Locations/{id}

    Get all / Post a location: http://ftmapi.northeurope.azurecontainer.io/api/locations
    Get / Put / Delete a spesific location: http://ftmapi.northeurope.azurecontainer.io/api/locations/{id}
    
MatchGoals / MatchGoals/{id]

    Get all / Post a matchgoal: http://ftmapi.northeurope.azurecontainer.io/api/matchgoals
    Get / Put / Delete a spesific matchgoal: http://ftmapi.northeurope.azurecontainer.io/api/matchgoals/{id}

    
MatchPositions / MatchPositions/{id} / MatchPositions{playerId}/{matchId}

    Get / Delete a players matchposition: http://ftmapi.northeurope.azurecontainer.io/api/matchpositions/{playerId}/{matchId}
    Put a matchposition: http://ftmapi.northeurope.azurecontainer.io/api/matchpositions/{id}
    Post a matchposition: http://ftmapi.northeurope.azurecontainer.io/api/matchpositions
    
Owners / Owners/{id}

    Get all / Post an owner: http://ftmapi.northeurope.azurecontainer.io/api/owners
    Get / Put / Delete a spesific owner: http://ftmapi.northeurope.azurecontainer.io/api/owners/{id}
 
People / People/{id}

    Get all / Post an person: http://ftmapi.northeurope.azurecontainer.io/api/people
    Get / Put / Delete a spesific person: http://ftmapi.northeurope.azurecontainer.io/api/people/{id}

Results / Results/{id} / Results/{teamId}/{matchId} / Results/{matchId}/{teamId}

    Get all results / Post a result: http://ftmapi.northeurope.azurecontainer.io/api/results
    Get a specific result: http://ftmapi.northeurope.azurecontainer.io/api/results/{teamId}/{matchId}
    Delete a result: http://ftmapi.northeurope.azurecontainer.io/api/results/{matchId}/{teamId}
    Put a result: http://ftmapi.northeurope.azurecontainer.io/api/results/{id}
    

Seasons / Seasons/{id}

    Get all seasons / Post a season: http://ftmapi.northeurope.azurecontainer.io/api/seasons
    Get / Put / Delete a spesific season: http://ftmapi.northeurope.azurecontainer.io/api/seasons/{id}

    
Teams / Teams/{id} / Teams/GetPlayers/{id}

    Get all teams / Post a season: http://ftmapi.northeurope.azurecontainer.io/api/teams
    Get / Put / Delete a spesific teams: http://ftmapi.northeurope.azurecontainer.io/api/teams/{id}
    Get all players in a spesific team: http://ftmapi.northeurope.azurecontainer.io/api/teams/GetPlayers/{id}
    
<!-- Authentication -->
## Authentication
The API is using JWT with Identidy. 

<!-- Entity Framework -->
## Entity Framework
The schematic structure is built with Entity Framework.

## Not completed requirements
Here are the following requirements that are not completed at this point:
* MISC-02 Safety
    * The system must also allow for the backup/restoration of the database through the administrator controls.
* MISC-03 Security
    * Enabling and using enviromental variables.
* USER-04: Manage personal watch list of favourite players and/or teams.
* ADMIN-05: Create a contact or any person. A person may have multiple means of contact.
* ADMIN-13: Assign goalt to match-player pairs. A person can score multiple goals in a match. 
* ADMIN-14: Record the match results per team-match pair.


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

Thanks to all who have contributed and helped us on this case!

* Mentor: Kjartan Sæle Lerøy
* Greg Linklater
* Dean von Schoultz

