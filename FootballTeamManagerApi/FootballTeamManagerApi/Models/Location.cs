﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class Location
    {
        public int Id { get; set; }

        public Address Address { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
