﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class MatchGoal
    {
        public int Id { get; set; }

        public Player Player { get; set; }

        public GoalType GoalType { get; set; }

        public Match Match { get; set; }

        public string Description { get; set; }

    }
}
