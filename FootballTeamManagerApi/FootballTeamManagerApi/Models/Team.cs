﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class Team
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Association Association { get; set; }

        public Coach Coach { get; set; }

        public Owner Owner { get; set; }

        public Location Location { get; set; }

    }
}
