﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class Contact
    {
        public int Id { get; set; }

        public Person Person { get; set; }

        public string ContactType { get; set; }

        public string ContactDetail { get; set; }
    }
}
