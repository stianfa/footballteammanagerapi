﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class GoalType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
