﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class Player
    {
        public int Id { get; set; }

        public Person Person { get; set; }

        public Team Team { get; set; }

        public string NormalPosition { get; set; }

        public string Number { get; set; }
    }
}
