﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class Result
    {
        public int MatchId { get; set; }

        public int TeamId { get; set; }

        public Match Match { get; set; }

        public Team Team { get; set; }

        public int Score { get; set; }

        public string MatchResult { get; set; }
    }
}

