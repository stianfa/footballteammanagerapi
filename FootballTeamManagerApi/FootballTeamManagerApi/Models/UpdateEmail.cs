﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class UpdateEmail
    {
        public string UserId { get; set; }
        public string NewEmail { get; set; }
    }
}
