﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class Owner
    {
        public int Id { get; set; }

        public Person Person { get; set; }
    }
}
