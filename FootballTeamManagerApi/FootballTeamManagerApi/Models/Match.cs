﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class Match
    {
        public int Id { get; set; }

        public Team HomeTeam { get; set; }

        public Team AwayTeam { get; set; }

        public Season Season { get; set; }

        public Location Location { get; set; }

        public DateTime Date { get; set; }
    }
}
