﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FootballTeamManagerApi.Models
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }        
       
        
        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<MatchPosition>()
                .HasKey(c => new { c.PlayerId, c.MatchId });

            modelbuilder.Entity<Result>()
                .HasKey(c => new { c.TeamId, c.MatchId });

            base.OnModelCreating(modelbuilder);
        }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public DbSet<Person> People { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Association> Associations { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Location> Locations { get; set; }

        public DbSet<Result> Results { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<MatchPosition> MatchPositions { get; set; }

        public DbSet<Match> Matches { get; set; }
        public DbSet<MatchGoal> MatchGoals { get; set; }
        public DbSet<GoalType> GoalTypes { get; set; }



    }
}
