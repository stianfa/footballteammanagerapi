﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FootballTeamManagerApi.Models
{
    public class MatchPosition
    {
        public int PlayerId { get; set; }
        public int MatchId { get; set; }

        public Player Player { get; set; }
        public Match Match { get; set; }

        public string Position { get; set; }
    }
}