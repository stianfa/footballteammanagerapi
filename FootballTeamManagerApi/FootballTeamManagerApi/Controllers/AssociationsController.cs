﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FootballTeamManagerApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace FootballTeamManagerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AssociationsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public AssociationsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Associations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Association>>> GetAssociations()
        {
            return await _context.Associations.ToListAsync();
        }

        // GET: api/Associations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Association>> GetAssociation(int id)
        {
            var association = await _context.Associations.FindAsync(id);

            if (association == null)
            {
                return NotFound();
            }

            return association;
        }

        // PUT: api/Associations/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAssociation(int id, Association association)
        {
            if (id != association.Id)
            {
                return BadRequest();
            }

            _context.Entry(association).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssociationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Associations
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Association>> PostAssociation(Association association)
        {
            _context.Associations.Add(association);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAssociation", new { id = association.Id }, association);
        }

        // DELETE: api/Associations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Association>> DeleteAssociation(int id)
        {
            var association = await _context.Associations.FindAsync(id);
            if (association == null)
            {
                return NotFound();
            }

            _context.Associations.Remove(association);
            await _context.SaveChangesAsync();

            return association;
        }

        private bool AssociationExists(int id)
        {
            return _context.Associations.Any(e => e.Id == id);
        }
    }
}
