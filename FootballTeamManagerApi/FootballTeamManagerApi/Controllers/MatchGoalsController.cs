﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FootballTeamManagerApi.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.AspNetCore.Authorization;

namespace FootballTeamManagerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MatchGoalsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MatchGoalsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/MatchGoals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MatchGoal>>> GetMatchGoals()
        {
            return await _context.MatchGoals.Include(m => m.GoalType)
                                                
                                            .Include(m => m.Player.Person.Address)
                                            .Include(m => m.Player.Team.Association)
                                            .Include(m => m.Player.Team.Coach.Person.Address)
                                            .Include(m => m.Player.Team.Owner.Person.Address)
                                            .Include(m => m.Player.Team.Location.Address)

                                            .Include(m => m.Match.HomeTeam.Association)
                                            .Include(m => m.Match.HomeTeam.Coach.Person.Address)
                                            .Include(m => m.Match.HomeTeam.Owner.Person.Address)
                                            .Include(m => m.Match.HomeTeam.Location.Address)

                                            .Include(m => m.Match.AwayTeam.Association)
                                            .Include(m => m.Match.AwayTeam.Coach.Person.Address)
                                            .Include(m => m.Match.AwayTeam.Owner.Person.Address)
                                            .Include(m => m.Match.AwayTeam.Location.Address)

                                            .Include(m => m.Match.Season)
                                            .Include(m => m.Match.Location)

                                            .ToListAsync();
        }

        // GET: api/MatchGoals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MatchGoal>> GetMatchGoal(int id)
        {
            var matchGoal = await _context.MatchGoals.Include(m => m.GoalType)

                                                     .Include(m => m.Player.Person.Address)
                                                     .Include(m => m.Player.Team.Association)
                                                     .Include(m => m.Player.Team.Coach.Person.Address)
                                                     .Include(m => m.Player.Team.Owner.Person.Address)
                                                     .Include(m => m.Player.Team.Location.Address)

                                                     .Include(m => m.Match.HomeTeam.Association)
                                                     .Include(m => m.Match.HomeTeam.Coach.Person.Address)
                                                     .Include(m => m.Match.HomeTeam.Owner.Person.Address)
                                                     .Include(m => m.Match.HomeTeam.Location.Address)

                                                     .Include(m => m.Match.AwayTeam.Association)
                                                     .Include(m => m.Match.AwayTeam.Coach.Person.Address)
                                                     .Include(m => m.Match.AwayTeam.Owner.Person.Address)
                                                     .Include(m => m.Match.AwayTeam.Location.Address)

                                                     .Include(m => m.Match.Season)
                                                     .Include(m => m.Match.Location)
                                                     .FirstOrDefaultAsync(m => m.Id == id);

            if (matchGoal == null)
            {
                return NotFound();
            }

            return matchGoal;
        }

        // PUT: api/MatchGoals/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMatchGoal(int id, MatchGoal matchGoal)
        {
            if (id != matchGoal.Id)
            {
                return BadRequest();
            }

            _context.Attach(matchGoal);

            IEnumerable<EntityEntry> unchangedEntities = _context.ChangeTracker.Entries().Where(x => x.State == EntityState.Unchanged);

            foreach (EntityEntry ee in unchangedEntities)
            {
                ee.State = EntityState.Modified;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MatchGoalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MatchGoals
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MatchGoal>> PostMatchGoal(MatchGoal matchGoal)
        {
            _context.Attach(matchGoal);

            IEnumerable<EntityEntry> unchangedEntities = _context.ChangeTracker.Entries().Where(x => x.State == EntityState.Unchanged);

            foreach (EntityEntry ee in unchangedEntities)
            {
                ee.State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMatchGoal", new { id = matchGoal.Id }, matchGoal);
        }

        // DELETE: api/MatchGoals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MatchGoal>> DeleteMatchGoal(int id)
        {
            var matchGoal = await _context.MatchGoals.FindAsync(id);
            if (matchGoal == null)
            {
                return NotFound();
            }

            _context.MatchGoals.Remove(matchGoal);
            await _context.SaveChangesAsync();

            return matchGoal;
        }

        private bool MatchGoalExists(int id)
        {
            return _context.MatchGoals.Any(e => e.Id == id);
        }
    }
}
