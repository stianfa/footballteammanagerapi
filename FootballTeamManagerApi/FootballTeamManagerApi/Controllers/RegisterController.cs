﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FootballTeamManagerApi.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace AspNetCore3JWT.Controllers
{
    [Route("api/[controller]")]
    public class RegisterController : Controller
    {

        private IServiceProvider serviceProvider;

        public RegisterController(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        [HttpPost]
        public Task<IdentityResult> Register([FromBody] Register newUser)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            ApplicationUser user = new ApplicationUser()
            {
                Email = newUser.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = newUser.Username,
                IsAdmin = false
            };

           return userManager.CreateAsync(user, newUser.Password);

        }
        [HttpGet]
        [Route("debug")]
        public List<IdentityUser> GetRegister()
        {
            var _context = serviceProvider.GetRequiredService<ApplicationDbContext>();

            return _context.Users.ToList();

        }
    }
}
