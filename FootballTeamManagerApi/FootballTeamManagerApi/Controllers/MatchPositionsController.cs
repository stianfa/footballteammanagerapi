﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FootballTeamManagerApi.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.AspNetCore.Authorization;

namespace FootballTeamManagerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MatchPositionsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MatchPositionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/MatchPositions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MatchPosition>>> GetMatchPositions()
        {
            return await _context.MatchPositions.Include(m => m.Player.Person.Address)
                                                .Include(m => m.Player.Team.Association)
                                                .Include(m => m.Player.Team.Coach.Person.Address)
                                                .Include(m => m.Player.Team.Owner.Person.Address)
                                                .Include(m => m.Player.Team.Location.Address)

                                                .Include(m => m.Match.HomeTeam.Association)
                                                .Include(m => m.Match.HomeTeam.Coach.Person.Address)
                                                .Include(m => m.Match.HomeTeam.Owner.Person.Address)
                                                .Include(m => m.Match.HomeTeam.Location.Address)

                                                .Include(m => m.Match.AwayTeam.Association)
                                                .Include(m => m.Match.AwayTeam.Coach.Person.Address)
                                                .Include(m => m.Match.AwayTeam.Owner.Person.Address)
                                                .Include(m => m.Match.AwayTeam.Location.Address)

                                                .Include(m => m.Match.Season)
                                                .Include(m => m.Match.Location)

                                                .ToListAsync();
        }

        // GET: api/MatchPositions/5
        [HttpGet("{playerId}/{matchId}")]
        public async Task<ActionResult<MatchPosition>> GetMatchPosition(int playerId, int matchId)
        {
            var matchPosition = await _context.MatchPositions.Include(m => m.Player.Person.Address)
                                                             .Include(m => m.Player.Team.Association)
                                                             .Include(m => m.Player.Team.Coach.Person.Address)
                                                             .Include(m => m.Player.Team.Owner.Person.Address)
                                                             .Include(m => m.Player.Team.Location.Address)

                                                             .Include(m => m.Match.HomeTeam.Association)
                                                             .Include(m => m.Match.HomeTeam.Coach.Person.Address)
                                                             .Include(m => m.Match.HomeTeam.Owner.Person.Address)
                                                             .Include(m => m.Match.HomeTeam.Location.Address)

                                                             .Include(m => m.Match.AwayTeam.Association)
                                                             .Include(m => m.Match.AwayTeam.Coach.Person.Address)
                                                             .Include(m => m.Match.AwayTeam.Owner.Person.Address)
                                                             .Include(m => m.Match.AwayTeam.Location.Address)

                                                             .Include(m => m.Match.Season)
                                                             .Include(m => m.Match.Location)
                                                             .FirstOrDefaultAsync(m => m.Player.Id == playerId && m.Match.Id == matchId);

            if (matchPosition == null)
            {
                return NotFound();
            }

            return matchPosition;
        }

        // PUT: api/MatchPositions/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut]
        public async Task<IActionResult> PutMatchPosition(MatchPosition matchPosition)
        {

            _context.Attach(matchPosition);

            IEnumerable<EntityEntry> unchangedEntities = _context.ChangeTracker.Entries().Where(x => x.State == EntityState.Unchanged);

            foreach (EntityEntry ee in unchangedEntities)
            {
                ee.State = EntityState.Modified;
            }



            await _context.SaveChangesAsync();


            return NoContent();
        }

        // POST: api/MatchPositions
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MatchPosition>> PostMatchPosition(MatchPosition matchPosition)
        {
            _context.Attach(matchPosition);

            IEnumerable<EntityEntry> unchangedEntities = _context.ChangeTracker.Entries().Where(x => x.State == EntityState.Unchanged);

            foreach (EntityEntry ee in unchangedEntities)
            {
                ee.State = EntityState.Modified;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MatchPositionExists(matchPosition.PlayerId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMatchPosition", new { id = matchPosition.PlayerId }, matchPosition);
        }

        // DELETE: api/MatchPositions/5
        [HttpDelete("{playerId}/{matchId}")]
        public async Task<ActionResult<MatchPosition>> DeleteMatchPosition(int playerId, int matchId)
        {
            var result = _context.MatchPositions.Find(playerId, matchId);

            if (result == null)
            {
                return NotFound();
            }

            _context.MatchPositions.Remove(result);
            await _context.SaveChangesAsync();

            return result;
        }

        private bool MatchPositionExists(int id)
        {
            return _context.MatchPositions.Any(e => e.PlayerId == id);
        }
    }
}
