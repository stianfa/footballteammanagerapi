﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FootballTeamManagerApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

using Newtonsoft.Json;

namespace FootballTeamManagerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UpdateUserController : ControllerBase
    {
        private IServiceProvider serviceProvider;

        public UpdateUserController(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        [HttpPut("email")]
        public async Task<ActionResult<IdentityUser>> UpdateUser(UpdateEmail emailInfo)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var _context = serviceProvider.GetRequiredService<ApplicationDbContext>();

            var user = await userManager.FindByIdAsync(emailInfo.UserId);

            if (user == null)
            {
                return NotFound();
            }
              
            user.Email = emailInfo.NewEmail;
            user.NormalizedEmail = emailInfo.NewEmail.ToUpper();

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPut("password")]
        public async Task<ActionResult<IdentityUser>> UpdatePassword(UpdatePassword passwordInfo)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var user = await userManager.FindByIdAsync(passwordInfo.UserId);

            if(user == null)
            {
                return NotFound();
            }

            var login = await userManager.CheckPasswordAsync(user, passwordInfo.OldPassword);
            if (login)
            {
                var token = await userManager.GeneratePasswordResetTokenAsync(user);
                var result = await userManager.ResetPasswordAsync(user, token, passwordInfo.NewPassword);
                if (result.Succeeded == true)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest(result);
                }
            }
            else
            {
                return BadRequest("Old password not correct");
            }
        }
    }
}