﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FootballTeamManagerApi.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.AspNetCore.Authorization;

namespace FootballTeamManagerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ResultsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ResultsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Results
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Result>>> GetResults()
        {
            return await _context.Results
                                 .Include(r => r.Team.Association)
                                 .Include(r => r.Team.Coach.Person.Address)
                                 .Include(r => r.Team.Owner.Person.Address)
                                 .Include(r => r.Team.Location.Address)

                                 .Include(r => r.Match.HomeTeam.Association)
                                 .Include(r => r.Match.HomeTeam.Coach.Person.Address)
                                 .Include(r => r.Match.HomeTeam.Owner.Person.Address)
                                 .Include(r => r.Match.HomeTeam.Location.Address)

                                 .Include(r => r.Match.AwayTeam.Association)
                                 .Include(r => r.Match.AwayTeam.Coach.Person.Address)
                                 .Include(r => r.Match.AwayTeam.Owner.Person.Address)
                                 .Include(r => r.Match.AwayTeam.Location.Address)

                                 .Include(r => r.Match.Season)

                                 .ToListAsync();
        }

        // GET: api/Results/5
        [HttpGet("{teamId}/{matchId}")]
        public async Task<ActionResult<Result>> GetResult(int teamId, int matchId)
        {

            
            var result = await _context.Results
                                 .Include(r => r.Team.Association)
                                 .Include(r => r.Team.Coach.Person.Address)
                                 .Include(r => r.Team.Owner.Person.Address)
                                 .Include(r => r.Team.Location.Address)

                                 .Include(r => r.Match.HomeTeam.Association)
                                 .Include(r => r.Match.HomeTeam.Coach.Person.Address)
                                 .Include(r => r.Match.HomeTeam.Owner.Person.Address)
                                 .Include(r => r.Match.HomeTeam.Location.Address)

                                 .Include(r => r.Match.AwayTeam.Association)
                                 .Include(r => r.Match.AwayTeam.Coach.Person.Address)
                                 .Include(r => r.Match.AwayTeam.Owner.Person.Address)
                                 .Include(r => r.Match.AwayTeam.Location.Address)
                                 
                                 .FirstOrDefaultAsync(r => r.Team.Id == teamId && r.Match.Id == matchId);

            if (result == null)
            {
                return NotFound();
            }

            return result;
        }

        // PUT: api/Results/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut]
        public async Task<IActionResult> PutResult(Result result)
        {
            _context.Attach(result);

            IEnumerable<EntityEntry> unchangedEntities = _context.ChangeTracker.Entries().Where(x => x.State == EntityState.Unchanged);

            foreach (EntityEntry ee in unchangedEntities)
            {
                ee.State = EntityState.Modified;
            }
          
            
            await _context.SaveChangesAsync();
            


            return NoContent();
        }

        // POST: api/Results
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Result>> PostResult(Result result)
        {

            _context.Attach(result);

            IEnumerable<EntityEntry> unchangedEntities = _context.ChangeTracker.Entries().Where(x => x.State == EntityState.Unchanged);

            foreach (EntityEntry ee in unchangedEntities)
            {
                ee.State = EntityState.Modified;
            }

            await _context.SaveChangesAsync();
                  

           return Ok(result);
        }

        // DELETE: api/Results/5
        [HttpDelete("{matchId}/{teamId}")]
        public async Task<ActionResult<Result>> DeleteResult(int matchId, int teamId)
        {
            var result =  _context.Results.Find(matchId, teamId);

            if (result == null)
            {
                return NotFound();
            }

            _context.Results.Remove(result);
            await _context.SaveChangesAsync();

            return result;
        }

    }
}
