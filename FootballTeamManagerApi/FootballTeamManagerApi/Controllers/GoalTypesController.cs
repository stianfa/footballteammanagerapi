﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FootballTeamManagerApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace FootballTeamManagerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GoalTypesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public GoalTypesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/GoalTypes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GoalType>>> GetGoalTypes()
        {
            return await _context.GoalTypes.ToListAsync();
        }

        // GET: api/GoalTypes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GoalType>> GetGoalType(int id)
        {
            var goalType = await _context.GoalTypes.FindAsync(id);

            if (goalType == null)
            {
                return NotFound();
            }

            return goalType;
        }

        // PUT: api/GoalTypes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGoalType(int id, GoalType goalType)
        {
            if (id != goalType.Id)
            {
                return BadRequest();
            }

            _context.Entry(goalType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GoalTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GoalTypes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<GoalType>> PostGoalType(GoalType goalType)
        {
            _context.GoalTypes.Add(goalType);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGoalType", new { id = goalType.Id }, goalType);
        }

        // DELETE: api/GoalTypes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<GoalType>> DeleteGoalType(int id)
        {
            var goalType = await _context.GoalTypes.FindAsync(id);
            if (goalType == null)
            {
                return NotFound();
            }

            _context.GoalTypes.Remove(goalType);
            await _context.SaveChangesAsync();

            return goalType;
        }

        private bool GoalTypeExists(int id)
        {
            return _context.GoalTypes.Any(e => e.Id == id);
        }
    }
}
