﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FootballTeamManagerApi.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace FootballTeamManagerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MatchesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Matches
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Match>>> GetMatches()
        {

            return await _context.Matches.Include(m => m.Season)
                                         .Include(m => m.HomeTeam.Association)
                                         .Include(m => m.HomeTeam.Coach.Person.Address)
                                         .Include(m => m.HomeTeam.Owner.Person.Address)
                                         .Include(m => m.HomeTeam.Location.Address)

                                         .Include(m => m.AwayTeam.Association)
                                         .Include(m => m.AwayTeam.Coach.Person.Address)
                                         .Include(m => m.AwayTeam.Owner.Person.Address)
                                         .Include(m => m.AwayTeam.Location.Address)

                                         .Include(m => m.Season)
                                         .Include(m => m.Location)

                                         .ToListAsync();
        }

        // GET: api/Matches/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Match>> GetMatch(int id)
        {
            var match = await _context.Matches.Include(m => m.Season)
                                            .Include(m => m.HomeTeam.Association)
                                            .Include(m => m.HomeTeam.Coach.Person.Address)
                                            .Include(m => m.HomeTeam.Owner.Person.Address)
                                            .Include(m => m.HomeTeam.Location.Address)

                                            .Include(m => m.AwayTeam.Association)
                                            .Include(m => m.AwayTeam.Coach.Person.Address)
                                            .Include(m => m.AwayTeam.Owner.Person.Address)
                                            .Include(m => m.AwayTeam.Location.Address)

                                            .Include(m => m.Season)
                                            .Include(m => m.Location)
                                            .FirstOrDefaultAsync(m => m.Id == id);

            if (match == null)
            {
                return NotFound();
            }

            return match;
        }

        // PUT: api/Matches/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMatch(int id, Match match)
        {
            if (id != match.Id)
            {
                return BadRequest();
            }

             _context.Attach(match);
            
            IEnumerable<EntityEntry> unchangedEntities = _context.ChangeTracker.Entries().Where(x => x.State == EntityState.Unchanged);

            foreach (EntityEntry ee in unchangedEntities)
            {
                ee.State = EntityState.Modified;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MatchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Matches
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Match>> PostMatch(Match match)
        {
            _context.Attach(match);

            IEnumerable<EntityEntry> unchangedEntities = _context.ChangeTracker.Entries().Where(x => x.State == EntityState.Unchanged);

            foreach (EntityEntry ee in unchangedEntities)
            {
                ee.State = EntityState.Modified;
            }

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMatch", new { id = match.Id }, match);
        }

        // DELETE: api/Matches/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Match>> DeleteMatch(int id)
        {
            var match = await _context.Matches.FindAsync(id);
            if (match == null)
            {
                return NotFound();
            }

            _context.Matches.Remove(match);
            await _context.SaveChangesAsync();

            return match;
        }

        private bool MatchExists(int id)
        {
            return _context.Matches.Any(e => e.Id == id);
        }
    }
}
