﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FootballTeamManagerApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.IdentityModel.Claims;

namespace FootballTeamManagerApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {

        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public AdminController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        [HttpGet]
        public string test()
        {
            return "you need to be logged in to see this";
        }

        // POST: api/Admin
        [HttpPost]
        public  async Task<IActionResult> SetUserToAdmin([FromBody] List<ApplicationUser> loggedInAndTarget)
        {

            var loggedInUser = await _context.ApplicationUsers.FindAsync(loggedInAndTarget[0].Id);
            var newAdminUser = await _context.ApplicationUsers.FindAsync(loggedInAndTarget[1].Id);

            if(loggedInUser != null && newAdminUser != null)
            {
                if (loggedInUser.IsAdmin)
                {
                    newAdminUser.IsAdmin = true;
                    await _context.SaveChangesAsync();
                    return Ok("Successfully updated the isAdmin-property of user: " + newAdminUser.UserName);
                }
                else
                {
                    return BadRequest("Logged in user is not an admin");
                }
            }

            return BadRequest("Couldn't find one of the users");
        }
    }
}